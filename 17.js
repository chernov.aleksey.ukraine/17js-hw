let student = createNewStudent();
console.log(student);
console.log(student.checkResult());
 
function createNewStudent() {
    const student = {
        _firstName: '',
        _lastName: '',
        grades: {},

        set firstName(value) {
            if (typeof value === "string" && value !== 0) {
                this._firstName = value;
                return this._firstName;
            } else while (typeof uName !== "string" || !isNaN(uName) || !uName || +uName === 0) {
                uName = prompt("Enter student's first name");
                }
            this._firstName = uName;
            return this._firstName;
        },

        set lastName(value) {
            if (typeof value === "string" && value !== 0) {
                this._lastName = value;
                return this._lastName;
            } else while (typeof uName !== "string" || !isNaN(uName) || !uName || +uName === 0) {
                uName = prompt("Enter student's last name");
                }
            this._lastName = uName;
            return this._lastName;
        },

        get firstName() {
            return this._firstName;
        },

        get lastName() {
            return this._lastName;
        },

        courseGrades: function () {
            coName = prompt("Enter the course name");
            coGrad = +prompt("Enter the course grade");
            while (coName && coGrad) {
            student.grades[coName] = coGrad;
            coName = prompt("Enter the course name");
            coGrad = +prompt("Enter the course grade"); 
            } 
        },
        checkResult: function () {
            answer = '';
            grFalse = false;
            for (let key in this.grades) {
                if (this.grades[key] < 4) {
                    grFalse = true;
                    return answer;
                }
            }
            if (!grFalse) {
                answer += `The student ${this.firstName} ${this.lastName} has been transfered to the next course` + "\n";
                let avGrad = 0;
                let corCount = 0;
                for (let key in this.grades) {
                    corCount++;
                    avGrad = avGrad + this.grades[key];
                }
                avGrad = avGrad / corCount;
                if (avGrad > 7) {
                answer +=  `The scholarship has been awarded to the student ${this.firstName} ${this.lastName}`;
                }
            }
            return answer;
        },

    };
    student.firstName = prompt("Enter student's first name");
    student.lastName = prompt("Enter student's last name");
    student.courseGrades();
    return student;
}

